<?php
require "bootstrap.php";

use Chatter\Models\User;

$app = new \Slim\App();

$app->get('/hello/{name}', function($request, $response, $args){
    return $response->write('Hello '.$args['name']);
 }); 

 $app->get('/users', function($request, $response, $args){
    $_user = new User(); //create new user
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'name'=>$usr->name,
            'phonenumber'=>$usr->phonenumber
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->get('/users/{id}', function($request, $response, $args){
    $_id = $args['id'];
    $user = User::find($_id);
       return $response->withStatus(200)->withJson($user);
    });
    

$app->post('/users', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name','');
    $phonenumber = $request->getParsedBodyParam('phonenumber','');
    $_user = new User();
    $_user->name = $name;
    $_user->phonenumber = $phonenumber;
    $_user->save();
    if($_user->id){
        $payload = ['user id: '=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->delete('/users/{id}', function($request, $response, $args){
    $user = User::find($args['id']);
    $user->delete();
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->put('/users/{user_id}', function($request, $response, $args){
    $username = $request->getParsedBodyParam('username','');
    $userphonenumber = $request->getParsedBodyParam('userphonenumber','');
    $_user = User::find($args['user_id']);
    //die("user id is " . $_user->id);
    $_user->name = $username;
    $_user->phonenumber = $userphonenumber;
    if($_user->save()){
        $payload = ['user_id'=>$_user->id,"result"=>"The user has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
         return $response->withStatus(400);
    }
});

$app->post('/users/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody();//מה שאני מקבל זה רק ג'ייסון והפקודה הופכת את פיילווד למערך 
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();